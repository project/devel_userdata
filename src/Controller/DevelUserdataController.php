<?php

namespace Drupal\devel_userdata\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Devel Userdata routes.
 */
class DevelUserdataController extends ControllerBase {

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The controller constructor.
   *
   * @param \Drupal\user\UserDataInterface $user_data
   *   The user data service.
   */
  public function __construct(UserDataInterface $user_data, ModuleHandlerInterface $module_handler) {
    $this->userData = $user_data;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.data'),
      $container->get('module_handler')
    );
  }

  /**
   * Builds the response.
   */
  public function build(AccountInterface $user) {

    $modules = $this->moduleHandler->getModuleList();

    $build['modules'] = [
      '#type' => 'details',
      '#title' => $this->t('Modules'),
      '#collapsible' => FALSE,
      '#open' => TRUE,
    ];

    foreach ($modules as $name => $modules) {
      $data = $this->userData->get($name, $user->id());
      if (!empty($data)) {

        $build['modules'][$name] = [
          '#type' => 'details',
          '#title' => $name,
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          '#open' => TRUE,
        ];

        foreach ($data as $key => $value) {

          $build['modules'][$name][$key] = [
            '#type' => 'details',
            '#title' => $key,
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
            '#open' => FALSE,
          ];

          $build['modules'][$name][$key]['value'] = [
            '#type' => 'item',
            '#markup' => '<pre>' . print_r($value, TRUE) . '</pre>'
          ];

        }
      }
    }

    return $build;
  }

}
